import React from 'react';
import '../../App.css'
import TopNavbar from '../../components/top-navbar';
import {Filter, Plus} from 'react-feather'
import Loader from '../../components/loader'

export default class Home extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            loading: false
        }
    }
    newApplication =()=>{
        this.setState({loading: true},()=>{
            setTimeout(()=>{
                this.setState({loading: false})
            }, 3000)
        })
    }
    render(){
        return(
            <div className="main-page">
                <TopNavbar hide={true} history={this.props.history}/>
                <div className="section-styled">
                    
                    <div className="flex-row-wrapper">
                    <div className="stepper-wrapper-inbox">
                        <div className="settings-side-menu-wrapper">
                            <div className="individual-settings-menu">General Settings</div>
                            <div className="individual-settings-menu setting-menu-active add-bottom-border">Security Settings</div>
                            <div className="individual-settings-menu">Inbox Settings</div>
                            <div className="individual-settings-menu">Notification Settings</div>
                        </div>
                    </div>
                    <div className="sub-section-styled-apply-inbox">
                        <h2>Settings</h2>
                    </div>
                    </div>
                </div>
            </div>
        )
    }
}