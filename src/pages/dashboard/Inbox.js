import React from 'react';
import '../../App.css'
import TopNavbar from '../../components/top-navbar';
import {XCircle, PlusCircle, HelpCircle, Trash2, Search, CornerUpLeft, Camera, Printer, Send} from 'react-feather'
import Loader from '../../components/loader'
import TextInput from '../../components/text-input';
import CKEditor from 'ckeditor4-react';
import html2canvas from 'html2canvas';

export default class Inbox extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            loading: false,
            isSearching: false,
            q: '',
            replying: false,
            taking: false
        }
    }
    setReplying=()=>{
        this.setState({replying: true})
    }
    notReplying=()=>{
        this.setState({replying: false})
    }

    takeScreenShot=()=>{
        this.setState({taking: true},async()=>{
            const canvas = await html2canvas(document.querySelector("#capture"), {
                useCORS: true,
                imageTimeout: 3000,
                allowTaint: true,
                backgroundColor: '#FFFFFF',
                scale: window.devicePixelRatio,
                height: window.outerHeight + window.innerHeight,
                windowHeight: window.innerHeight,
            });
            const base64image = await canvas.toDataURL("image/jpeg", 0.9);
            const src = encodeURI(base64image);
            document.getElementById('screen').src = src
            const screenshot = document.querySelector('#screen').src;
            //BUG: this does not work yet
            window.open(screenshot, '_blank');

            await this.setState({taking: false});
        })
        
    }
    newApplication =()=>{
        this.setState({loading: true},()=>{
            setTimeout(()=>{
                this.setState({loading: false})
            }, 3000)
        })
    }
    search =()=>{
        this.setState({isSearching: true},()=>{
            setTimeout(()=>{
                this.setState({isSearching: false})
            }, 3000)
        })
    }
    onSearchInput=(e)=>{
        this.setState({q: e.target.value})
    }
    render(){
        return(
            <div className="main-page">
                <TopNavbar placeholder="Search with Application ID ..." history={this.props.history}/>
                <div className="section-styled">
                    <div className="flex-row-wrapper">
                    <div className="stepper-wrapper-inbox">
                        <div className="settings-side-menu-wrapper">
                            {!this.state.replying?
                                <>
                                <div className="individual-inbox-menu">
                                    <div className="topper-items-wrapper">
                                        <div className="individual-inbox-id">
                                            <div className="new-inbox-notif"></div>
                                            ID: IRB5346
                                        </div>
                                        <div className="individual-inbox-timestamp">12:52 PM</div>
                                    </div>
                                    <div className="message-summary">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                    </div>
                                </div>
                                <div className="individual-inbox-menu inbox-menu-active">
                                    <div className="topper-items-wrapper">
                                        <div className="individual-inbox-id">
                                            <div className="new-inbox-notif"></div>
                                            ID: IRB5346
                                        </div>
                                        <div className="individual-inbox-timestamp">12:52 PM</div>
                                    </div>
                                    <div className="message-summary">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                    </div>
                                </div>
                                <div className="individual-inbox-menu">
                                    <div className="topper-items-wrapper">
                                        <div className="individual-inbox-id">
                                            <div className="new-inbox-notif"></div>
                                            ID: IRB5346
                                        </div>
                                        <div className="individual-inbox-timestamp">12:52 PM</div>
                                    </div>
                                    <div className="message-summary">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                    </div>
                                </div>
                                

                                <div className="pagination-wrapper-full">
                                    <div className="individual-pagination-item">&laquo;</div>
                                    <div className="individual-pagination-item pagination-active">1</div>
                                    <div className="individual-pagination-item">2</div>
                                    <div className="individual-pagination-item">3</div>
                                    <div className="individual-pagination-item">&raquo;</div>
                                </div>
                                </>
                            :
                                <div className="reply-wrapper">
                                    <div className="close-icon-wrapper">
                                        <XCircle onClick={this.notReplying} size={25} className="close-icon"/>
                                    </div>
                                    <div className="reply-to"><strong className="strong-color-override">To:</strong> Institutional Review Board</div>
                                    <div className="reply-subject"><strong className="strong-color-override">Subject:</strong> ID: IRB5346 - SSD and Cloud Computing Research</div>
                                    {/*The Wysiwyg editor goes here*/}
                                    <div className="reply-box-wrapper">
                                        <CKEditor 
                                        className="ckeditor-class"
                                        config={ {
                                            ckfinder:{uploadUrl: "/uplaodImageFromEditor"},
                                            toolbar: [ [ 'Bold', 'Italic', 'NumberedList', 'BulletedList', 'Image', 'Link', 'Unlink', 'SpecialChar', 'Undo', 'Maximize' ] ]
                                        } }
                                        />
                                    </div>
                                    {/*Add style to the attachment maybe a button for a modal or something with a drag and drop space*/}
                                    <div className="reply-attachment-wrapper">
                                        Attachments: <span className="add-attachment-text"><PlusCircle className="attachment-icon"/> Add</span> <span className="attachemnt-buttons-seperator"></span> <span className="add-attachment-text"><Trash2 className="attachment-icon"/> Remove</span>
                                    </div>
                                    <div className="reply-buttons-wrapper">
                                        <div className="inbox-sub-icon">
                                            <HelpCircle size={20} className="reply-icon"/> Help
                                        </div>
                                        <div className="inbox-sub-icon">
                                            <Send size={20} className="reply-icon"/> Send
                                        </div>
                                        
                                    </div>
                                </div>
                            }
                        </div>
                        
                    </div>
                    <div className="sub-section-styled-apply-inbox">
                        <div className="inbox-icon-box">
                            <div className="inbox-icon-sub-box">
                                <div onClick={this.setReplying} className={this.state.replying? "inbox-sub-icon active-icon" : "inbox-sub-icon"} >
                                <CornerUpLeft size={20} className="reply-icon"/> Reply
                                </div>
                                <div onClick={this.takeScreenShot} className="inbox-sub-icon">
                                {this.state.taking?
                                <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="20"
                                height="20"
                                fill="none"
                                stroke="currentColor"
                                strokeLinecap="round"
                                strokeLinejoin="round"
                                strokeWidth="2"
                                className="feather feather-clock reply-icon"
                                viewBox="0 0 24 24"
                                >
                                <circle cx="12" cy="12" r="10" />
                                <path d="M12 6L12 12 16 14" className="svg-loader" />
                                </svg>
                                :
                                <Camera size={20} className="reply-icon"/>
                                }
                                 Screenshot
                                </div>
                                <div className="inbox-sub-icon">
                                <Printer size={20} className="reply-icon"/> Print
                                </div>
                            </div>
                        </div>
                        <div id="capture" className="inbox-message-detail">
                            <div className="inbox-message-detail-title-date-time-wrapper">
                                <h3 className="inbox-message-title">ID: IRB5346</h3>
                                <h4 className="inbox-message-title">SSD and Cloud Computing in the 21st Century</h4>
                                <div className="individual-inbox-timestamp">12:52 PM</div>
                            </div>
                            <div className="inbox-message-body">
                                {/*Dangerously set inner HTML here because data comes from WYSIWYG editor that saves html and image links*/}
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                <img className="inline-inbox-message-image" src="../../blog-2-17-2017-InsertAPicturePlaceholderInPublisher.png" alt="message-inbox-image" />
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            <img src="" id="screen" width="350" height="auto"/>
                            </div>
                            <div className="inbox-message-reply-box">

                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        )
    }
}