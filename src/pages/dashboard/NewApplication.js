import React from 'react';
import '../../App.css'
import TopNavbar from '../../components/top-navbar';
import {Plus, HelpCircle, Search} from 'react-feather'
import Loader from '../../components/loader'
import TextInput from '../../components/text-input';

export default class NewApplication extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            loading: false,
            isSearching: false,
            q: ''
        }
    }
    newApplication =()=>{
        this.setState({loading: true},()=>{
            setTimeout(()=>{
                this.setState({loading: false})
            }, 3000)
        })
    }
    search =()=>{
        this.setState({isSearching: true},()=>{
            setTimeout(()=>{
                this.setState({isSearching: false})
            }, 3000)
        })
    }
    onSearchInput=(e)=>{
        this.setState({q: e.target.value})
    }
    render(){
        return(
            <div className="main-page">
                <TopNavbar back={true} backUrl="/" placeholder="Enter Application Number ..." history={this.props.history}/>
                <div className="section-styled">
                    <div className="flex-row-wrapper">
                    <div className="stepper-wrapper-inbox">
                        <div className="settings-side-menu-wrapper">
                                
                        </div>
                        
                    </div>
                    <div className="sub-section-styled-apply-inbox">
                        <h2>New Application</h2>
                    </div>
                    </div>
                </div>
            </div>
        )
    }
}