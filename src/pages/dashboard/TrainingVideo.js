import React from 'react'
import TopNavbar from '../../components/top-navbar'

export default class TrainingVideo extends React.Component{
    render(){
        return(
            <div className="main-page">
                <TopNavbar placeholder="Search for an Application ..." history={this.props.history}/>
                <div className="section-styled">
                    <div className="sub-section-styled-actions">
                       
                    </div>
                    <div className="flex-row-wrapper">
                    <div className="stepper-wrapper hide">
                    </div>
                    <div className="sub-section-styled">
                        <h2>NSU IRB Researcher Training Video</h2>
                       
                        <div className="player-container">
                            
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        )
    }
}