import React from 'react';
import '../../App.css'
import TopNavbar from '../../components/top-navbar';
import {Filter, Plus} from 'react-feather'
import Loader from '../../components/loader'

export default class Home extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            loading: false
        }
    }
    newApplication =()=>{
        this.setState({loading: true},()=>{
            setTimeout(()=>{
                this.setState({loading: false},()=>{
                    this.props.history.push('/apply');
                })
            }, 1500)
        })
    }
    render(){
        return(
            <div className="main-page">
                <TopNavbar placeholder="Search for an Application ..." history={this.props.history}/>
                <div className="section-styled">
                    <div className="sub-section-styled-actions">
                        <div className="action-rounder">
                            <Filter className="icon-spacer" size={20}/>
                            Filter
                        </div>
                        <div className="action-rounder green-color-override" onClick={this.newApplication}>
                            {this.state.loading?
                            <Loader />
                            :
                            <>
                            <Plus className="icon-spacer" size={20}/>
                            New Application
                            </>
                            }
                        </div>
                    </div>
                    <div className="flex-row-wrapper">
                    <div className="stepper-wrapper hide">
                    </div>
                    <div className="sub-section-styled">
                        <h2>My Applications</h2>
                        <div className="individual-application-wrapper">
                            <div className="title-and-id-wrapper">
                            <p className="individual-application-heading">SSD and Cloud Computing in the 21st Century</p>
                            <p className="application-id">ID: IRB78452455</p>
                            </div>
                            <div className="application-description">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                            </div>
                            <div className="title-and-id-wrapper">
                            <p className="application-status">Status: <div className="status-indicator-approved"></div> Approved</p>
                            <p className="individual-application-view-more">View More</p>
                            </div>
                        </div>
                        <div className="individual-application-wrapper">
                            <div className="title-and-id-wrapper">
                            <p className="individual-application-heading">Role of Genetics in Cancer</p>
                            <p className="application-id">ID: IRB78452455</p>
                            </div>
                            <div className="application-description">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                            </div>
                            <div className="title-and-id-wrapper">
                            <p className="application-status">Status: <div className="status-indicator-pending"></div> Pending</p>
                            <p className="individual-application-view-more">View More</p>
                            </div>
                        </div>
                        <div className="individual-application-wrapper">
                            <div className="title-and-id-wrapper">
                            <p className="individual-application-heading">Special Gene for Happiness</p>
                            <p className="application-id">ID: IRB78452455</p>
                            </div>
                            <div className="application-description">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                            </div>
                            <div className="title-and-id-wrapper">
                            <p className="application-status">Status: <div className="status-indicator-pending"></div> Pending</p>
                            <p className="individual-application-view-more">View More</p>
                            </div>
                        </div>
                        <div className="individual-application-wrapper">
                            <div className="title-and-id-wrapper">
                            <p className="individual-application-heading">Blah Blah Blah Blah</p>
                            <p className="application-id">ID: IRB78452455</p>
                            </div>
                            <div className="application-description">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                            </div>
                            <div className="title-and-id-wrapper">
                            <p className="application-status">Status: <div className="status-indicator-rejected"></div> Rejected</p>
                            <p className="individual-application-view-more">View More</p>
                            </div>
                        </div>
                    
                        <div className="pagination-wrapper">
                            <div className="individual-pagination-item">&laquo;</div>
                            <div className="individual-pagination-item pagination-active">1</div>
                            <div className="individual-pagination-item">2</div>
                            <div className="individual-pagination-item">3</div>
                            <div className="individual-pagination-item">4</div>
                            <div className="individual-pagination-item">&raquo;</div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        )
    }
}