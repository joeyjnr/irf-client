import React from 'react';
import Loader from './loader'

const PrimaryButton =(props)=>{
    return(
        <button className="primary-button" onClick={props.onClick}>
            {props.loading?
                <Loader />
                :
                <>
                {props.title}
                </>
            }
        </button>
    )
}

export default PrimaryButton;