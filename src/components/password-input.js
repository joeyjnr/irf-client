import React from 'react';
import {Eye, EyeOff} from 'react-feather'


export default class PasswordInput extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            show: false
        }
    }
    showPass =()=>{
        this.setState({show: true})
    }
    hidePass =()=>{
        this.setState({show: false})
    }
    render(){
    return(
        <div className="password-input-wrapper">
            <input type={this.state.show? "text": "password"} className="password-input" placeholder={this.props.placeholder? this.props.placeholder: "Enter Password"} style={this.props.styles} onChange={(e)=>this.props.dataChange(e)}/>
            {!this.state.show?
            <Eye onClick={this.showPass} className="password-toggle" color="#757575"/>
            :
            <EyeOff onClick={this.hidePass} className="password-toggle" color="#757575"/>
            }
        </div>
    )
    }
}
