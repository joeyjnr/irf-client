import React from 'react';
//Import validation wrappers here

const TextInput = (props)=>{
    return(
        <div className="email-input-wrapper">
            <input type="email" autoCorrect="off" autoCapitalize="off" spellCheck="false" className="email-input" placeholder={props.placeholder} style={props.styles} onChange={(e)=>props.dataChange(e)}/>
        </div>
    )
}

export default TextInput;