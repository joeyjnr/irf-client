import React from 'react';
import {ChevronDown, Search, ArrowLeft} from 'react-feather';
import NotVerified from './not-verified'
import Loader from './loader'
import '../App.css'

export default class TopNavbar extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            loading: false
        }
    }
    search = async() =>{
        //For some reason it needed to be a promise
        //So I used async await instead of using the setState callback function
        await this.setState({loading: true})
        await setTimeout(async()=>{
            await this.setState({loading: false})
        }, 3000)
    }
    goBack=()=>{
        this.props.history.push('/');
    }
    verifyNow=()=>{
        this.props.history.push('/training-video');
    }
    render(){
        const {loading} = this.state
        //Pull the history.location from this.props to know what page is currently being rendered
        const {location} = this.props.history
        return(
            <>
            <div className="top-navbar">
                    <div className="consume-space">
                        {this.props.back?
                        <div onClick={this.goBack} className="back-wrapper">
                            <ArrowLeft className="back-arrow" size={22}/> Back
                        </div>
                        :null
                        }
                        {!this.props.hide?
                        <div className="search-input-wrapper">
                            {loading?
                            <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="24"
                            height="24"
                            fill="none"
                            stroke="currentColor"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            strokeWidth="2"
                            className="feather feather-clock search-input-icon"
                            viewBox="0 0 24 24"
                            >
                            <circle cx="12" cy="12" r="10" />
                            <path d="M12 6L12 12 16 14" className="svg-loader" />
                            </svg>
                            :
                            <Search size="24" onClick={this.search} className="search-input-icon"/>
                            }
                            <input className="search-input" type="text" placeholder={this.props.placeholder || "Search ..."}/>
                        </div>
                        :null
                        }
                    </div>
                    <div className="right-navbar-items">
                        <div className="profile-info">
                            <img src="../../avatar.png" className="top-navbar-avatar" alt="avatar"/>
                            <p className="profile-name">Paingha Alagoa</p>
                            <ChevronDown color="#757575" size={28}/>
                        </div>
                    </div>
                </div>
                <NotVerified verify={this.verifyNow}/>
                </>
        )
    }
}
TopNavbar.defaultProps = {
    back: false
}
/*
**
TODO: List of needed to do in this component
1) Add search field - DONE
2) Add search button - DONE
3) Functionality to Know what page is currently rendered
**
*/