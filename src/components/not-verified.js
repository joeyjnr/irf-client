import React from 'react'
import {AlertTriangle} from 'react-feather'

const NotVerified = (props)=>{
    return(
        <div className="not-verified-wrapper">
            <div className="not-verified-info-wrapper">
            <AlertTriangle size={30} className="warning-icon"/>
            <h4>Watch the Training Video to Get Started</h4>
            </div>
            <div onClick={props.verify} className="resolve-outline-btn">
                Resolve Now
            </div>
        </div>
    )
}

export default NotVerified;