import React from 'react';
//Import validation wrappers here

const EmailInput = (props)=>{
    return(
        <div className="email-input-wrapper">
            <input type="email" autoCorrect="off" autoCapitalize="off" spellCheck="false" className="email-input" placeholder="Enter Email" style={props.styles} onChange={(e)=>props.dataChange(e)}/>
        </div>
    )
}

export default EmailInput;