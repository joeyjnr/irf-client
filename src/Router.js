import React from 'react'
//import { connect } from 'react-redux';
import {BrowserRouter, Route, Switch} from 'react-router-dom';

import Register from './pages/account/Register'
import Login from './pages/account/Login'
import Forgot from './pages/account/Forgot'
import NewPassword from './pages/account/NewPassword'
import App from './App'
import Home from './pages/dashboard/Home'
import Inbox from './pages/dashboard/Inbox'
import Settings from './pages/dashboard/Settings'
import NewApplication from './pages/dashboard/NewApplication'
import TrainingVideo from './pages/dashboard/TrainingVideo'



const PrivateArea = (props)=>{
    return(
        <>
        {props.children}
        </>
    )
}


export const AppRouter = props => (
    <BrowserRouter>
        <Switch>
            <Route path="/register" component={Register}/>
            <Route path="/login" component={Login}/>
            <Route path="/forgot-password" component={Forgot}/>
            <Route path="/change-password" component={NewPassword}/>
            <PrivateArea>
                <App>
                    <Route exact path="/" component={Home}/>
                    <Route path="/inbox" component={Inbox}/>
                    <Route path="/settings" component={Settings} />
                    <Route path="/apply" component={NewApplication} />
                    <Route path="/training-video" component={TrainingVideo} />
                </App>
            </PrivateArea>
        </Switch>
    </BrowserRouter>
);