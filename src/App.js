import React from 'react';
import './App.css';
//import Login from './pages/account/Login'
//import Register from './pages/account/Register'
//import Forgot from './pages/account/Forgot'
//import NewPassword from './pages/account/NewPassword'
import Sidebar from './components/sidebar'

//Wrapper Component for other
export default class App extends React.Component {
  componentDidMount(){
    console.log("app mounted fine")
  }
  render(){
  return (
    <div className="wrapper-row">
    <Sidebar history={this.props.history}/>
    {this.props.children}
    </div>
  );
  }
}
